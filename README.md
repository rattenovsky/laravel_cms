# Iguana recruitment task

## Getting started

### Demo

Demo is available [here](https://shielded-ravine-87286.herokuapp.com)

### Requirements

* **PHP** >= 7.1.3
* **GD Library**
* **mailtrap** account (for password reset)
* **mysql** database (local or remote)

### Launch the project

*(Assuming you've [installed Laravel](https://laravel.com/docs/5.5/installation))*

Fork this repository, then clone your fork, and run this in your newly created directory:

``` bash
composer install
```

Next you need to make a copy of the `.env.example` file and rename it to `.env` inside your project root.

Run the following command to generate your app key:

```bash
php artisan key:generate
```

Then migrate database:
```bash
php artisan migrate --seed
```

Then start your server:

```bash
php artisan serve
```

Your project is now up and running!

### Configure the project

Edit the `.env` file to get the application connected to the correct database and mailtrap server:

```bash
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=USERNAME_FROM_MAILTRAP
MAIL_PASSWORD=PASSWORD_FROM_MAILTRAP

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=YOUR_DATABASE
DB_USERNAME=YOUR_USERNAME
DB_PASSWORD=YOUR_PASSWORD
```

You may have to restart your server.
