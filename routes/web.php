<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index')->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::delete('/article/remove/{id}', 'ArticleController@remove')->name('article-remove')->middleware('auth');
Route::get('/article/edit/{id}', 'ArticleController@edit')->name('article-edit')->middleware('auth');
Route::get('/article/create', 'ArticleController@create')->name('article-create')->middleware('auth');
Route::post('/article/add', 'ArticleController@add')->name('article-add')->middleware('auth');
Route::post('/comment/add', 'CommentController@add')->name('comment-article')->middleware('auth');
Route::get('/article/{id}', 'ArticleController@show')->name('article');
Route::get('/user/{id}', 'UserController@show')->name('user');