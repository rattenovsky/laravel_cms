@extends('layouts.app')

@section('content')
    <div class="container bg-white rounded shadow-sm py-3">
      <div class="row">
        <div class="col">
          <h3>{{isset($article) ? 'Edit' : 'Create'}} article</h3>
        </div>
      </div>
      <form class="p-3" method="POST" action="{{route('article-add')}}">
      @csrf
      @if (isset($article))
          <input type="hidden" name="article" value="{{$article->id}}">
      @endif
        <div class="form-group row align-items-center">
          <label for="titleInput" class="col-sm-2">Title</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="titleInput" placeholder="Title of article" name="title" value="{{$article->title ?? ''}}">
          </div>
        </div>
        <div class="form-group row align-items-center">
            <label for="contentInput" class="col-sm-2">Content</label>
            <div class="col-sm-10">
              <textarea name="content" id="contentInput" class="form-control" rows="10" placeholder="Content of article">{!!$article->content ?? ''!!}</textarea>
            </div>
          </div>
        <div class="form-group row align-items-center">
            <label for="thumbnailInput" class="col-sm-2">Thumbnail</label>
            <div class="col-sm-10">
              <img src="https://loremflickr.com/400/300?random={{rand(1,1024)}}" alt="" class="img-fluid shadow rounded">
              <input type="hidden" name="thumbnail" value="https://loremflickr.com/800/600?random={{rand(1,1024)}}">
            </div>
        </div>
        <div class="row">
          <div class="col">
            <button type="submit" class="btn btn-outline-success float-right">{{isset($article) ? 'Edit' : 'Create'}} article</button>
          </div>
        </div>
      </form>
    </div>
@endsection