@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row align-items-center justify-content-center mb-5">
      <div class="col-sm-12 col-md-8 col-lg-6">
        <div class="row no-gutters">
          <div class="col">
            <h1 style="font-size: 3.5em; font-weight: bold">
                  {{$article->title}}
            </h1>
            <p class="text-muted">
                <small>
                  By <a href="{{route('user', ['id'=>$article->user->id])}}">{{$article->user->name}}</a> on {{date('F d, Y',$article->created_at->getTimestamp())}}
                </small>
              </p>
          </div>
        </div>
        @auth
        @if(Auth::user()->id == $article->user->id)
          <div class="row no-gutters" style="font-size: .75em">
            <div class="col">
                <a href="{{route('article-edit', ['id' => $article->id])}}" class="mr-3"><i class="far fa-edit"></i> Edit</a>
                <form action="{{route('article-remove', ['id' => $article->id])}}" method="POST" class="d-inline" id="delete-form">
                    @csrf
                    @method('DELETE')
                    <a onclick="event.preventDefault();
                    document.getElementById('delete-form').submit();" href="#" class="inline"><i class="far fa-trash-alt"></i> Remove</a>
                </form>
            </div>
          </div>
        @endif
       @endauth
        <div class="horizontal-separator"></div>
        <div class="row no-gutters">
            <div class="col">
              {!!$article->content!!}
            </div>
          </div>
      </div>
    </div>
    <div class="row no-gutters mb-2 align-items-center justify-content-center">
        <div class="col text-center">
          <img src="{{$article->thumbnail}}" class="img-fluid rounded mb-2" />
        </div>
      </div>
    <hr />
    <div class="row">
      <div class="col">
        <div class="row no-gutters">
          <h3>Comments
            @if ($article->comments->count() > 0)
               ({{$article->comments->count()}}) 
            @endif
          </h3>
        </div>
        @if ($article->comments->count() == 0)
        <h5 class="text-muted">No comments yet</h5>
        @else
           @foreach ($article->comments as $comment)
               <div class="row no-gutters my-3 justify-content-center align-items-center">
                 <div class="col-1 text-center">
                    <img src="{{new YoHang88\LetterAvatar\LetterAvatar(implode(" ",explode('.', $comment->user->name)), 'circle')}}" alt="{{$comment->user->name}} avatar" class="img-fluid" />
                 </div>
                 <div class="col-11">
                   <p class="my-0 py-0">{!!$comment->content!!}</p>
                   <p class="text-muted">
                     <small><a href="{{route('user', ['id' => $comment->user->id])}}">{{$comment->user->name}}</a> &middot; {{date('d M Y, H:i',$comment->created_at->getTimestamp())}}</small>
                   </p>
                 </div>
               </div>
           @endforeach 
        @endif
        @auth
           <div class="row no-gutters">
             <div class="col">
              @if (Auth::user()->id == $article->user->id)
              <div class="alert alert-danger">
                  You cannot comment your own article.
                </div>
              @else
              <hr />
              <h4>Add comment</h4>
              <form action="{{ route('comment-article') }}" method="POST">
                @csrf
                <input type="hidden" name="article" value="{{$article->id}}">
                <textarea name="content" rows="3" class="form-control"></textarea>
                <button type="submit" class="btn btn-success btn-sm float-right my-2">Add comment</button>
              </form>
              @endif
             </div>
           </div>   
        @endauth
        @guest
            <div class="row no-gutters">
              <div class="col">
                <div class="alert alert-danger">
                  You must be logged in to comment articles.
                </div>
              </div>
            </div>
        @endguest
      </div>
    </div>
  </div>
@endsection