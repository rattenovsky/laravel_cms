@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col text-center">
          <img src="{{new YoHang88\LetterAvatar\LetterAvatar(implode(" ",explode('.', $user->name)), 'circle', 128)}}" alt="{{$user->name}} avatar" class="img-fluid py-4" />
          <h3><strong>{{$user->name}}</strong></h3>
      </div>
    </div>
    <div class="row mt-5 align-items-center justify-content-center">
      <div class="col-4">
        <div class="row no-gutters text-center">
          <div class="col"><h3><strong>{{$user->articles->count()}}</strong></h3><p class="text-muted"><small>ARTICLES</small></p></div>
          <div class="col"><h3><strong>{{$user->comments->count()}}</strong></h3><p class="text-muted"><small>COMMENTS</small></p></div>
        </div>
      </div>
    </div>
    <div class="row bg-white shadow-sm rounded p-3">
      <div class="col-lg-6 col-md-12"><h3 class="d-inline-block">Articles</h3><a href="{{ route('article-create') }}" class="d-inline-block ml-2">Add article</a>
        <div class="row no-gutters">
          @foreach ($user->articles as $article)
            <div class="col-lg-6 col-md-12 p-2">
              <div class="card p-2 pb-0 border-0 bg-body shadow">
                  <img src="{{$article->thumbnail}}" class="card-img-top rounded" alt="{{$article->title}} - thumbnail">
                  <div class="card-body">
                    <h5 class="card-title h6"><strong>{{$article->title}}</strong></h5>
                    <p class="card-text">{{str_split($article->content, 120)[0]}}...</p>
                    <small><a href="{{ route('article', ['id'=>$article->id]) }}" class="card-link">Read more</a></small>
                    &middot; <small><a href="{{ route('article-edit', ['id'=>$article->id]) }}" class="card-link">Edit</a></small>
                    <form action="{{route('article-remove', ['id' => $article->id])}}" method="POST" class="d-inline" id="delete-form">
                            @csrf
                            @method('DELETE')
                            &middot; <small>
                            <a onclick="event.preventDefault();
                            document.getElementById('delete-form').submit();" href="#" class="inline card-link">Delete</a></small>
                    </form>
                  </div>
                </div>
            </div>
          @endforeach
        </div>
      </div>
      <div class="col-lg-6 col-md-12"><h3>Comments</h3>
        <ul style="list-style-type: none">
          @foreach ($user->comments as $comment)
            <li>
              <h6>Commented <a href="{{ route('article', ['id'=>$comment->article->id]) }}"><strong>#{{$comment->article->id}}</strong></a> on {{date('F d, Y H:i', $comment->created_at->getTimestamp())}}</h6>
              <p class="text-muted pl-2"><small>{!!$comment->content!!}</small></p>
            </li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
@endsection