@extends('layouts.app')

@section('content')
<div class="container">
    @foreach ($articles as $article)
    <div class="row mb-3">
        <div class="col">
            <div class="card mb-2 border-0 shadow-sm p-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                    <img src="{{$article->thumbnail}}" class="card-img shadow" alt="{{$article->title}} thumbnail">
                    </div>
                    <div class="col-md-8">
                    <div class="card-body pt-0">
                        <h3 class="card-title">{{$article->title}}</h3>
                        <p class="card-text">
                            {{str_split($article->content, 255)[0]}}...
                        </p>
                        <p class="card-text"><small class="text-muted">By <a href="{{route('user', ['id' => $article->user->id])}}"><img src="{{new YoHang88\LetterAvatar\LetterAvatar(implode(" ",explode('.', $article->user->name)), 'circle', 16)}}"/> {{$article->user->name}}</a> on {{date('F d, Y H:i',$article->created_at->getTimestamp())}}
                            @if ($article->created_at->getTimestamp() !== $article->updated_at->getTimestamp())
                             &middot; Last updated {{$article->updated_at->diffForHumans()}}
                            @endif
                        </small></p>
                        <p class="card-text"><small class="text-muted">
                            {{$article->comments->count()}} Comment{{$article->comments->count() == 1 ? '' : 's'}}
                            @if ($article->comments->count() > 0)
                            &middot; Latest by <a href="{{url('/user/'.$article->comments->first()['user']['id'])}}">{{$article->comments->first()['user']['name']}}</a> on {{date('F d, Y H:i',$article->comments->first()['created_at']->getTimestamp())}}
                            @endif
                        </small></p>
                        <a href="{{url('/article/'.$article->id)}}" class="btn btn-outline-primary btn-sm">Read more...</a>
                        @auth
                        @if ($article->user->id === Auth::user()->id)
                        <form action="{{route('article-remove', ['id' => $article->id])}}" method="POST" class="d-inline-block">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-outline-danger btn-sm">Delete article</button>
                        </form>
                        @endif
                        @endauth
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
<div class="container">
    <div class="row">
        <div class="d-flex col justify-content-center align-items-center">
            {{$articles->links()}}
        </div>
    </div>
</div>
@endsection
