<?php

use App\Article;
use Faker\Generator as Faker;
use Faker\Provider\Internet as Internet;
use Faker\Provider\Lorem as Lorem;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(Article::class, function (Faker $faker) {
  $faker->addProvider(new Lorem($faker));
    return [
        'title' => $faker->sentence(),
        'content' => $faker->text(600),
        'thumbnail' => 'https://loremflickr.com/800/600?random='.$faker->numberBetween(1,1024),
        'user_id' => $faker->numberBetween(1, App\User::count())
    ];
});
