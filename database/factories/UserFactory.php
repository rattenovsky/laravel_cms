<?php

use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Faker\Provider\Internet as Internet;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(User::class, function (Faker $faker) {
    $faker->addProvider(new Internet($faker));
    return [
        'name' => $faker->userName,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => bcrypt('kaczka'),
        'remember_token' => Str::random(10),
    ];
});
