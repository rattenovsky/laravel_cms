<?php

use App\Comment;
use Faker\Generator as Faker;
use Faker\Provider\Lorem as Lorem;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(Comment::class, function (Faker $faker) {
  $faker->addProvider(new Lorem($faker));
    return [
        'content' => $faker->text(150),
        'article_id' => $faker->numberBetween(1, App\Article::count()),
        'user_id' => $faker->numberBetween(1, App\User::count())
    ];
});
