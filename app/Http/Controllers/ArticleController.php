<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class ArticleController extends Controller
{
    public function show($id)
    {
        $article = Article::find($id);
        if (!$article) abort(404);
        return view('article.show', [
            "article" => $article
        ]);
    }
    public function remove(Request $request, $id)
    {
        $article = Article::find($id);
        if (!$article) {
            return redirect()->back()->with('error', 'Article with given ID doesn\'t exist.');
        } else {
            if ($article->user->id !== Auth::user()->id) {
                return redirect()->back()->with('error', 'You must be author of the article to delete it.');
            }
            $deletedRows = Article::where('id', $id)->delete();
            if ($deletedRows) {
                return strpos(URL::previous(), '/article/') ? redirect()->route('home')->with('success', 'Article '.$article->id.' has been deleted.') : redirect()->back()->with('success', 'Article '.$article->id.' has been deleted.');
            } else {
                return redirect()->back()->with('error', 'Something went wrong.');
            }
        }
    }

    public function create()
    {
        return view('article.create');
    }

    public function add(Request $request)
    {
        $content = $request->post('content');
        $title = $request->post('title');
        $thumbnail = $request->post('thumbnail');
        $author = Auth::user()->id;
        if (strlen($content) <= 0 || strlen($title) <= 0) {
            return redirect()->back()->with('error', 'Content/title cannot be blank.')->withInput();
        }
        $article = $request->post('article') ? Article::find($request->post('article')) : new Article;
        $article->content = $content;
        $article->title = $title;
        $article->thumbnail = $thumbnail;
        $article->user_id = $author;
        if ($article->save()) {
            $str = $request->post('article') ? 'edited.' : 'added.';
            return redirect()->route('home')->with('success', 'Article has been '.$str);
        } else {
            return redirect()->back()->with('error', 'Something went wrong.')->withInput();
        }
    }
    public function edit($id) 
    {   
        $article = Article::find($id);
        if (!$article) {
            return redirect()->back()->with('error', 'Article with given ID doesn\'t exist.');
        }
        if ($article->user->id !== Auth::user()->id) {
            return redirect()->back()->with('error', 'You must be author of the article to edit it.');
        }
        return view('article.create', [
            "article" => $article
        ]);
    }
}
