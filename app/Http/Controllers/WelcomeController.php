<?php

namespace App\Http\Controllers;
use App\Article as Article;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index()
    {
        $articles = Article::with(['user', 'comments.user'])->paginate(6);
        return view('welcome', [
            "articles" => $articles
        ]);
    }
}
