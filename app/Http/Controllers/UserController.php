<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Comment;
use App\User;

class UserController extends Controller
{
    public function show($id)
    {
        $user = User::find($id);

        if (!$user) abort(404);

        return view('user.show', [
            "user" => $user
        ]);
    }
}
