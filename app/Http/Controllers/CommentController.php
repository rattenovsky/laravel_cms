<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Comment;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function add(Request $request)
    {
        $article = Article::find($request->post('article'));
        $content = $request->post('content');
        if (!$article) {
            return redirect()->back()->with('error', 'Article you want to comment doesn\'t exist.');
        } else {
            if (strlen($content) <= 0) {
                return redirect()->back()->with('error', 'Comment cannot be blank.');
            }
            $comment = new Comment;
            $comment->content = $content;
            $comment->article_id = $article->id;
            $comment->user_id = Auth::user()->id;
            if ($comment->save()) {
                return redirect()->back()->with('success', 'Comment has been added.');
            } else {
                return redirect()->back()->with('error', 'Something went wrong.');
            }
        }
    }
}
